﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M03Uf3FitxersRamosVictor
{
    class FilesConfig
    {
        public void LlegirFitxer(string path, ref string name)
        {
            string text = File.ReadAllText(path);
            string[] textSplit = text.Split('\n', ':');
            name = textSplit[1];
        }
        public void PrintarName(string path, string name)
        {
            string text = File.ReadAllText(path);
            Console.WriteLine(text + "" + name);
        }

        public void FindFile(string target, string fileToSearch)
        {
            DirectoryInfo carpeta = new DirectoryInfo(target);
            DirectoryInfo[] dires = carpeta.GetDirectories();
            IEnumerable<FileInfo> files = carpeta.EnumerateFiles("*.*", SearchOption.TopDirectoryOnly);
            foreach (FileInfo file in files)
            {
                if (file.Name == fileToSearch)
                {
                    Console.WriteLine(file.DirectoryName + " " + file.Name);
                }
            }

            if (Directory.Exists(target))
            {
                foreach (var subdir in dires)
                {
                    string combine = Path.Combine(target, subdir.Name);
                    FindFile(combine, fileToSearch);
                }
            }
        }
    }
}
