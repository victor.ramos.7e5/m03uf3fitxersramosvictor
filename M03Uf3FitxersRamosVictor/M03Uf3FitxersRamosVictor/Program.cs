﻿using System;
using System.Collections.Generic;
using System.IO;

namespace M03Uf3FitxersRamosVictor
{
    class Program
    {
        static void Main(string[] args)
        {
            Program p = new Program();
            p.Inici();
        }
        void Inici()
        {
            bool fin;
            do
            {
                Console.Clear();
                MostrarMenu();
                string opcion = Console.ReadLine();
                fin = CallExercice(opcion);
            } while (!fin);

        }
        void MostrarMenu()
        {
            Console.WriteLine("[1]: ParellSenar");
            Console.WriteLine("[2]: Inventary");
            Console.WriteLine("[3]: WordCount");
            Console.WriteLine("[4]: ConfigFile");
            Console.WriteLine("[4]: FindPath");
            Console.WriteLine("[0]: Sortir");
        }

        bool CallExercice(string opcion)
        {
            Console.Clear();
            switch (opcion)
            {
                case "1":
                    ParellSenar();
                    break;
                case "2":
                    Inventario();
                    break;
                case "3":
                    WordCount();
                    break;
                case "4":
                    ConfifFile();
                    break;
                case "5":
                    FindPath();
                    break;
                case "0":
                    return true;

                default:
                    Console.WriteLine("Esta opció no existeix");
                    break;
            }

            return false;
        }

        void ParellSenar()
        {
            Dades d = new Dades();
            List<int> parell = new List<int>();
            List<int> senar = new List<int>();
            string pathNumbers = @"..\..\..\..\Files\Numbers.txt";
            foreach (string line in File.ReadLines(pathNumbers))
            {
                int num = d.ConvertStringToInt(line);
                if (Isparell(num))
                {
                    parell.Add(num);
                }
                else
                {
                    senar.Add(num);
                }
            }
            CreateFile(parell, @"..\..\..\..\Files\parell.txt");
            CreateFile(senar, @"..\..\..\..\Files\senar.txt");

        }
        public bool Isparell(int checkNum)
        {
            if ((checkNum % 2) == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public void CreateFile(List<int> contentFile, string nameFile)
        {
            StreamWriter streamWriter = new StreamWriter(nameFile);
            foreach (var s in contentFile)
            {
                streamWriter.WriteLine(s);
            }
            streamWriter.Close();
        }

        void Inventario()
        {
            string path = @"..\..\..\..\Files\Inventory.txt";
            if (File.Exists(path))
            {
                File.Delete(path);
                File.Create(path);
                Console.WriteLine("Se borra y lo crea");
            }
            else
            {
                File.Create(path);
                
            }
            Console.ReadLine();
            Ejercicio(path);
        }
        void Ejercicio(string path)
        {
            string lineToText = LeerPreguntas();
            AppendLineToInventory(lineToText, path);

        }
        public string LeerPreguntas()
        {
            Dades d = new Dades();
            string line = d.Respuestas("Introdueix el nom") + ", ";
            line += d.Respuestas("Introdueix el model") + ", ";
            line += d.Respuestas("Introdueix la MAC") + ", ";
            line += d.RespuestasAnyo("Any de fabricacio").ToString();
            return line;
        }

        public void AppendLineToInventory(string line,string path)
        {
            
            using (StreamWriter sw = File.AppendText(path))
            {
                sw.WriteLine(line);
            }

        }

        public void WordCount()
        {
            Dades d = new Dades();
            string path = @"..\..\..\..\Files\wordcount.txt";
            var text = ReadFileTextToList(path);
            string llegirParaula;
            do
            {
                llegirParaula = d.Respuestas("Introdueix una paraula per buscar al text");
                string[] split_text = llegirParaula.Split(' ');

                for (int i = 0; i < split_text.Length; i++)
                {
                    int countWord = 0;
                    foreach (string word in text)
                    {
                        if (word.ToLower() == split_text[i])
                        {
                            countWord++;
                        }
                    }
                    Console.WriteLine(split_text[i] + ": " + countWord);
                }
            } while (llegirParaula != "END");
        }
        public List<String> ReadFileTextToList(string path)
        {

            string[] readFile = File.ReadAllLines(path);
            string[] split = readFile[0].Split(' ', '.', ',');
            List<string> secretWords = new List<string>();
            foreach (var s in split)
            {
                secretWords.Add(s);
            }
            return secretWords;
        }

        public void ConfifFile()
        {
            Dades d = new Dades();
            FilesConfig f = new FilesConfig();
            string name = " ";
            string path = @"..\..\..\..\Files\lang\config.txt";
            string pathEsp = @"..\..\..\..\Files\lang\lng_es.txt";
            string pathCat = @"..\..\..\..\Files\lang\lng_cat.txt";
            f.LlegirFitxer(path, ref name);
            string idioma = d.Respuestas("Idioma [cat / es] ?").ToLower();
            if (idioma == "cat")
            {
                f.PrintarName(pathCat, name);
            }
            else if (idioma == "es")
            {
                f.PrintarName(pathEsp, name);
            }
            else
            {
                f.PrintarName(pathCat, name);
            }

        }
        public void FindPath()
        {
            Dades d = new Dades();
            FilesConfig f = new FilesConfig();
            string nomFitxer = d.Respuestas("Quin és el nom del fitxer a cercar?");
            string path = d.Respuestas("Escriu el nom d'una ruta a una carpeta:");
            f.FindFile(path, nomFitxer);
        }

    }
}
